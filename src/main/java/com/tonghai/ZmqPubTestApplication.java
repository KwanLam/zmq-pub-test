package com.tonghai;

import com.google.gson.Gson;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ZmqPubTestApplication {

    static final int HWM = 300000;
    static final int INTERVAL = 500;
    static final String PUBLISHER_ADDRESS = "tcp://0.0.0.0:33987";
    static final String PUBLISH_CHANNEL = "zmq-pub-test";

    public static void main(String[] args) {
        ArgumentParser parser = createArgumentParser();
        Namespace namespace = null;

        try {
            namespace = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        assert namespace != null;

        String address = PUBLISHER_ADDRESS;
        String channelToPublish = PUBLISH_CHANNEL;
        String argsAddress = namespace.getString("address");
        String argsChannel = namespace.getString("channel");

        if (argsAddress != null) {
            address = argsAddress;
        }
        if (argsChannel != null) {
            channelToPublish = argsChannel;
        }

        System.out.printf("Published on %s with channel %s%n", address, channelToPublish);

        Thread publisherThread = createPublisher(address, channelToPublish);
        publisherThread.start();
    }

    private static Thread createPublisher(final String address, final String channelToPublish) {
        return new Thread(() -> {
            try (ZContext context = new ZContext()) {
                final ZMQ.Socket publisher = context.createSocket(SocketType.PUB);
                int sequence = 0;
                Gson gson = new Gson();

                publisher.bind(address);
                publisher.setHWM(HWM);

                while (true) {
                    Thread.sleep(INTERVAL);

                    System.out.println(now() + "\tSending message with sequence: " + sequence);

                    publisher.sendMore(channelToPublish.getBytes(ZMQ.CHARSET));
                    publisher.send(gson.toJson(new String[]{"16808510", "170", "1607483136633", "68223", String.valueOf(sequence), "15000000", "33", "1", "0", "2", "0"}));

                    sequence++;
                }
            } catch (Exception e) {
                System.out.println("Error from createPublisher()");
                e.printStackTrace();
            }
        });
    }

    private static String now() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        return sdf.format(new Date());
    }

    private static ArgumentParser createArgumentParser() {
        ArgumentParser parser = ArgumentParsers.newFor("ZmqPubTest")
                .build()
                .defaultHelp(true)
                .description("ZmqPubTest");

        parser.addArgument("-addr", "--address")
                .help("tcp://0.0.0.0:33667")
                .required(false);
        parser.addArgument("-chl", "--channel")
                .help("zmq-pub-test")
                .required(false);

        return parser;
    }
}
